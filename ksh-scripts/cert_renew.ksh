#!/bin/ksh

# This script takes one parameter which is a FQDN and attempts to
# install given certificate in a destination file. Afterwards it
# reloads given web server.
# Example run: ./cert_renew.ksh example.com
# Source: https://devopsiarz.pl/bash/tutorial-kurs-pisania-dobrych-skryptow-bash-wstep-dobre-praktyki-shellcheck/

# Exit script if a command returns non-zero exit code 
set -e;

# Unset parameters are considered as an error resulting in exit
set -u;

FQDN=$1; # eg. example.com
# Don't have to handle situation when FQDN wasn't submitted because
# of `-u` flag
NGINX_CERT_PATH="/home/${USER}/certs";
RELOAD_CMD="/usr/bin/doas nginx -s reload";
USER_DIR="/home/${USER}";
LOCK_DIR="${USER_DIR}/acme-lock-dir";
DIR_CMD_CREATE="/bin/mkdir -p"
DIR_CMD_REMOVE="/bin/rm -r"

# Environment variables don't get passed from parent shell
ENVIRONMENTS="${USER_DIR}/.profile";
. ${ENVIRONMENTS};

remove_lock_dir()
{
    if [ -d "${LOCK_DIR}" ]; then
        ${DIR_CMD_REMOVE} "${LOCK_DIR}" || {
            # Route this echo to stderr
            echo "Cannot remove lock dir: ${LOCK_DIR}" >&2 ;
            exit 1;
        }
    fi
}

trap remove_lock_dir INT HUP KILL TERM QUIT;

if [ ! -d "${USER_DIR}" ]; then
    echo "Working directory: ${USER_DIR} doesn't exist." >&2 ;
    exit 1;
fi

if [ ! -d "${NGINX_CERT_PATH}" ]; then
    echo "Certificates directory: ${NGINX_CERT_PATH} doesn't exist." >&2 ;
    exit 1;
fi

if [ -d "${LOCK_DIR}" ]; then
    echo "Lock dir exists: ${LOCK_DIR} - another instance running or not exited properly" >&2 ;
    exit 1;
fi

# Create lock and exit if error
${DIR_CMD_CREATE} "${LOCK_DIR}" || {
    echo "Cannot create lock dir: ${LOCK_DIR}" >&2 ;
    exit 1;
}


# Copy certificate files to the destination and reload HTTP server
acme.sh --install-cert \
    -d ${FQDN} \
    -d www.${FQDN} \
    --key-file ${NGINX_CERT_PATH}/${FQDN}/privkey.pem \
    --fullchain-file ${NGINX_CERT_PATH}/${FQDN}/fullchain.pem \
    --reloadcmd "${RELOAD_CMD}" || {
        echo "Failed to run command." >&2 ;
        remove_lock_dir;
        exit 1;
    }

remove_lock_dir;
echo "OK";
